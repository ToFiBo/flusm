# FluSM: FLUxes from Soil Moisture 

FluSM code is part of **Fluxes from Soil Moisture Measurements (FluSM v1.0). A Data-driven Water Balance Framework for Permeable Pavements**, which currently is under review for the journal Geoscientific Model Development (https://doi.org/10.5194/gmd-2020-188). 
Refer to this publication for detailed information on the underlying theory of FluSM.

+ **Language:** FluSM was written in Python 2.7.14, but is compatible with python 3.x.x
+ **License:** FluSM is provided under the terms of the GNU General Public License v3.0. Therefore FluSM 
is provided without warranty of any kind
+ **Dependencies:** FluSM Dependencies are specified within the module FluSM.py

<br>

## Structure
FluSM consists of the following 4 individual python modules:

1. **FluSM.py**: This is the main module where all calculations are performed. All remaing modules are imported into this module.
2. **surfaceWaba.py**: In this module, all routines for calculating the surface water balance are defined
3. **soilWaba.py**: In this module, all routines for calculating the soil water balance are defined
4. **mapping.py**: In this module, the surface and the soil water balances are linked to obtain the complete water balance


Documentation on the individual functions used by FluSM is provided by docstrings within the code

<br>


## Control File
The file controleFile.csv is needed within the FluSM folder. In this file, the paths to all input files and the path where to save the output must be specified. 
All specified paths must exist before running FluSM. Subfolders are created during the execution of FluSM. 

+ (a): path_inputParams
+ (b): folder_output
+ (c): path_soilMoisture
+ (d): path_et0_d
+ (e): path_et0_h
+ (f): path_wetDryCycles
+ (g): path_rain

Furthermore, parameters controlling the FluSM calculations must be specified within this file. 
Comment lines within controlFile.csv (marked by #) provide information on the individual input files and the controls of FluSM which must be specified within this file

<br>

## Input Parameters
The file inputParams.csv must exist at the path specified by (a). FluSM is calculated for each plot specified within this file. 
For each plot a sensor must be specified, for which the soil water balance is calculated for. 
Data for this sensor must be incuded within the soil moisture file (c), indicated by an equally named column. 
Furthermore, the parameters for each plot must be specified within the file inputParams.csv. 
Comment lines within inputParams.csv (marked by #) provide further information on the individual inputs.


<br>

## Exemplary input files
The FluSM input for the case study is provided as an input example. The data originates 
from <https://freidok.uni-freiburg.de/data/149321> and is described in detail by *Schaffitel 
A., Schuetz, T., Weiler M.: A distributed soil moisture, temperature and infiltrometer data set for permeable pavements and green spaces* (Earth Syst. Sci. Data, 12, 501–517, 2020; https://doi.org/10.5194/essd-12-501-2020)

<br>

## FluSM Output
The output of FluSM consists of time series for the surface water balance, the soil water balance and the complete water balance. 
Thereby, the time series contain a column for each plot specified within inputParams.csv.
