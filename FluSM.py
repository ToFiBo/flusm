# -*- coding: utf-8 -*-
'''
======================================================================
FluSM: Fluxes from Soil Moisture measurements
A Python code to derive water fluxes from soil moisture measurements.
======================================================================

written with Python 2.7.15, compatible with Python 3.x.x

@author: Axel Schaffitel
@paper: Schaffitel, A., Schuetz, T.and Weiler, M. 
        A data-driven water balancing approach to derive Fluxes from Soil Moisture measurements. A case study for permeable pavements.
        Prepared for submission to Geoscientific model develompment in 2019

@dependencies:
        FluSM is provided under the terms of the GNU General Public License v3.0. 
        Therefore FluSM is provided without warranty of any kind

@dependencies:
        numpy >= 1.14.3
        pandas >= 0.23.0
        matplotlib >= 2.1.2
        scipy >=1.1.0
        seaborn >=0.9.0
@input:
        needed input files are described in the file controlFile.txt, where to path to each file must be specified

@output:
        FluSM returns time series for all water fluxes with a temporal resolution of 1 hour
        All files saved to the output folder specified in the file controlFile.txt
        
@documentation:        
        see the file readme.txt for an overview
        the individual functions of FluSM are documented by docstrings provided within the code
        inputs and specifications needed within the files controlFile.txt and inputParams.txt are explained by comment lines (marked by #) within the files

@expemlaryData:
        !!{einfuegen DOI Data Repository, Referenz Paper1}!! 


'''
from __future__ import print_function 

import os
import sys 
from datetime import datetime
import numpy as np
import pandas as pd

import surfaceWaba as wbSurf
import soilWaba as wbSoil
import mapping as mapp


dateConverter=lambda x : datetime.strptime(x, '%Y-%m-%d %H:%M:%S')
#===============================================================================
# dataImports
#===============================================================================
controlFile=pd.read_csv('./controlFile.csv', sep='\t', comment='#', index_col=0, header=None).iloc[:,0]
outputFolder=controlFile['folder_output']
inputParams=pd.read_csv(controlFile['path_inputParams'], sep='\t', comment='#', index_col=0)
soiMoi_df=pd.read_csv(controlFile['path_soilMoisture'], sep='\t', comment='#', converters={0:dateConverter},index_col=0)
et0_d=pd.read_csv(controlFile['path_et0_d'], sep='\t', comment='#', converters={0:lambda x: datetime.strptime(x, '%Y-%m-%d')},index_col=0, squeeze=True)
et0_h=pd.read_csv(controlFile['path_et0_h'], sep='\t', comment='#', converters={0:dateConverter}, index_col=0,  squeeze=True)
wetDryCycles=pd.read_csv(controlFile['path_wetDryCycles'], sep='\t', comment='#', converters={0:dateConverter},index_col=0)
rain_10m=pd.read_csv(controlFile['path_rain'], sep='\t', converters={0:dateConverter}, index_col=0,  names=['prec'], squeeze=True,)
rain_10m[(rain_10m>30)|(rain_10m<0)]=np.NaN
rain_1h=rain_10m.resample('h', closed='right', label='right').sum().iloc[:-1]

#------------------------------------------------------------------------------ 
# create output folders
for folder in ['surfaceWB', 'soilWB', 'completeWB', 'mapping', 'sSurf']:
    if folder not in os.listdir(outputFolder):
        os.mkdir(outputFolder+'/'+folder)
        if folder== 'eval' or folder =='mapping':
            os.mkdir(outputFolder+'/'+folder+'/pdf')  
        if folder=='surfaceWB':
            os.mkdir(outputFolder+'/'+folder+'/10min')

            
    

#------------------------------------------------------------------------------ 
# test if input data is correct
for sensor in inputParams['sensor'].tolist():
    if sensor not in soiMoi_df.columns.tolist():
        sys.exit('no soil moisture data provided for sensor {0:s} (inputParams.csv)'.format(sensor))

start=rain_10m.index[0]
end=rain_10m.index[-1]
for i in [soiMoi_df, et0_d, et0_h, wetDryCycles]:
    if i.index[0]!=start:
        sys.exit('time series do not start at same point in time')
    if i.index[-1].date() !=end.date():
        sys.exit('time series do not end at same date \nall time series must end with date {0:s}'.format(end.strftime('%Y-%m-%d')))

#------------------------------------------------------------------------------ 
# Fill missing entries in timeSeries with NaN
rain_10m=rain_10m.reindex(pd.date_range(start=start, end=end, freq='10min'))
soiMoi_df=soiMoi_df.reindex(pd.date_range(start=start, end=end, freq='h'))
et0_h=et0_h.reindex(pd.date_range(start=start, end=end, freq='h'))
et0_d=et0_d.reindex(pd.date_range(start=start, end=end, freq='d'))
wetDryCycles_d=wetDryCycles.reindex(pd.date_range(start=start, end=end, freq='d'))

outputParams=pd.DataFrame()
#===============================================================================
# FluSM step 1: determination of Ssurf
#     1a): grouping of rain events according to event sums (resolution specified by resolution_rainEvents)
#     1b): calculation of the max water content reaction for each rain event (wcReactn_events)
#     1c): calculation of the median of (1b) for each eventSum_group
#     1d): derivation of sSurf from (1c) by using the threshold specified by thresh_wcReact
#===============================================================================
#'''
thresh_wcReact=float(controlFile['thresh_wcReact']) # threshold used for (1d)
resolution_rainEvents=.5    # resolution [mm] used for grouping rain event sums (1a). Default 0.5 mm                            

#------------------------------------------------------------------------------
# (1a) 
eventSummary=rain_1h.groupby(wetDryCycles['wetDryCycle']).sum().to_frame()
binsPrec=np.arange(0, eventSummary['prec'].max()+1, resolution_rainEvents)    
eventSummary['eventSum_group']=pd.cut(eventSummary['prec'], binsPrec, labels=binsPrec[1:]).astype(float)

#------------------------------------------------------------------------------ 
# (1b), (1c), (1d) 
for plot in inputParams.index.tolist():
    sensor = inputParams.loc[plot,'sensor']
    eventSummary['wcReact']=soiMoi_df.loc[wetDryCycles['event_ind']==1, sensor].groupby(wetDryCycles['wetDryCycle']).apply(lambda x: x.max()-x.iloc[0]) #(1b)
    wcReactn_events=eventSummary.groupby('eventSum_group')['wcReact'].median() #(1c)
    outputParams.loc[plot, 'Csurf']=wcReactn_events.loc[wcReactn_events>thresh_wcReact].index[0]-resolution_rainEvents #(1d)


print ('~~~~~~~~~~~~~~~~~~')
print ('Csurf calculated\n')

#===============================================================================
# 2/4 calculation of the surface waterBalance
#    2a): calculation of the change of the surface storage over time (dSsurf_dt)
#    2b): calculation of the surface runoff (ro_10m)
#    2c): calculation of the infiltration (infiltr_10m)
#===============================================================================
#'''
for plot in inputParams.index:
    wbSurf_10min=pd.DataFrame()
    icap=inputParams.loc[plot, 'icap']
    Csurf_plot=outputParams.loc[plot, 'Csurf']
    
    #------------------------------------------------------------------------------ 
    # (2a), (2b), (2c)
    wbSurf_10min['dSsurf']=wbSurf.calc_dSsurf_dt(Csurf_plot, rain_10m, wetDryCycles, et0_h)  #(2a)
    wbSurf_10min['ro']=wbSurf.calc_ro(wbSurf_10min['dSsurf'], rain_10m, icap)    #(2b)
    wbSurf_10min['infiltr']=wbSurf.calc_infiltr(wbSurf_10min['dSsurf'], rain_10m, icap)  #(2c)
    wbSurf_10min.to_csv(outputFolder+'/surfaceWB/10min/'+ plot+'.csv', sep='\t')
    #------------------------------------------------------------------------------
    # aggregation to hourly values, & saving as .csv file
    wbSurf_h=wbSurf_10min.resample('h', closed='right', label='right').sum()
    wbSurf_h.to_csv(outputFolder+'/surfaceWB/'+ plot+'.csv', sep='\t')
    
print ('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
print ('surface water balance calculated\n')
#'''

#===============================================================================
# 3/4 soil water balance 
#    3a): selection of times for fitting the drainage model (3a1) and fitting the drainage model (3a2)
#    3b): prediction of the drainage loss from measured soil moisture
#    3c): calculation of the soil water balance by closure of the water balance
#===============================================================================
#'''
constraint1=controlFile['constraint1']
constraint2=controlFile['constraint2']
allObs_drainModel=controlFile['allObs_drainModel']
thresh_et0=float(controlFile['thresh_et0'])


for plot in inputParams.index.tolist():
    sensor=inputParams.loc[plot, 'sensor']
    soiMoi_ts=soiMoi_df[sensor]
    theta_s=soiMoi_ts.max()
    theta_r=soiMoi_ts.min()
    wbSurf_df=pd.read_csv(outputFolder+'/surfaceWB/'+plot+'.csv', sep='\t', converters={0:dateConverter}, index_col=0)
    sSurf_state=wbSurf_df['dSsurf'].cumsum()
    
    #------------------------------------------------------------------------------ 
    # (3a), (3b), (3c)
    drainLoss=wbSoil.get_drainageLoss(et0_d, soiMoi_ts, wetDryCycles, thresh_et0=thresh_et0, theta_s=theta_s, theta_r=theta_r) #(3a1)
    drainModel=wbSoil.fit_drainageModel(drainLoss=drainLoss, allObs_drainModel=allObs_drainModel)    #(3a2)
    drainPredict=wbSoil.predict_drainLoss(soiMoi_ts=soiMoi_ts, drainModel=drainModel, theta_s=theta_s, theta_r=theta_r) #(3b)
    wbSoil_df=wbSoil.calc_soilWaba(soiMoi_ts=soiMoi_ts, drainPredict=drainPredict, sSurf_state=sSurf_state, wetDryCycles=wetDryCycles, constraint1=constraint1, constraint2=constraint2) #(3c)

    #------------------------------------------------------------------------------ 
    # saving the soil water balance to .csv file
    wbSoil_df.to_csv(outputFolder+'/soilWB/'+plot+'.csv', sep='\t')
    outputParams.loc[plot, 'ks']=drainModel['ks']
    outputParams.loc[plot, 'B']=drainModel['B']
    outputParams.loc[plot, 'sigma_ks']=drainModel['sigma_ks']
    outputParams.loc[plot, 'sigma_B']=drainModel['sigma_B']
    outputParams.loc[plot, 'RMSE']=drainModel['RMSE']

print ('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
print ('soil water balance calculated\n')
plot='CP15'
#'''
#===============================================================================
# 4/4 linking the soil and the surface water balance by 
#     mapping the infiltration gains calculated by the soil module on the infiltration calculated by the surface module
#===============================================================================
#'''
for plot in inputParams.index:
    #------------------------------------------------------------------------------ 
    # creation of the dataFrame for mapping
    wbSurf_df=pd.read_csv(outputFolder+'/surfaceWB/'+plot+'.csv', sep='\t', converters={0:dateConverter}, index_col=0)
    wbSoil_df=pd.read_csv(outputFolder+'/soilWB/'+plot+'.csv', sep='\t', converters={0:dateConverter}, index_col=0)
    mappingDf=pd.concat([wetDryCycles, wbSurf_df, wbSoil_df], axis=1)
    mappingDf['eSurf']=(mappingDf.loc[mappingDf['dSsurf']<0, 'dSsurf']*-1)  #emptying of the surface storage is surface evaporation
    mappingDf['eSurf'].fillna(0, inplace=True)
    mappingDf['date']=mappingDf.index.strftime('%Y-%m')
    
    #------------------------------------------------------------------------------ 
    # linking of the water balances
    wbCompl=mappingDf.groupby('date').apply(mapp.map_linScaling)
    wbCompl.reset_index(level=0, drop=True, inplace=True)
    wbCompl=wbCompl.reindex(pd.date_range(start=wbCompl.index[0], end=wbCompl.index[-1], freq='h'))    
    wbCompl['rain']=rain_1h

    #------------------------------------------------------------------------------ 
    #  saving to .csv file
    wbCompl.to_csv(outputFolder+'/completeWB/'+plot+'.csv', sep='\t')
    
    #------------------------------------------------------------------------------ 
    # Median scaling factor
    outputParams.loc[plot, 'soilDepth_med']=wbCompl.dropna(subset=['dSsoil'])['scalingFact'].resample('m').mean().median()*100
    outputParams.loc[plot, 'soilDepth_std']=wbCompl.dropna(subset=['dSsoil'])['scalingFact'].resample('m').mean().std()*100
    
    #----------------------------------------------------------- 
    #bias in closing the water balance
    biasDf=wbCompl.copy().dropna(subset=['dSsoil'])
    outputParams.loc[plot, 'bias']=(biasDf['rain'].sum()-biasDf[['drainage', 'dSsoil', 'eSoil', 'dSsurf', 'eSurf', 'ro']].sum().sum())/biasDf['rain'].sum()
outputParams.T.to_csv(outputFolder+'/params.csv', sep='\t')
print ('~~~~~~~~~~~~~~~')
print ('step 4 done\n')
#'''


#===============================================================================
# sumarize water fluxes over the entire measuring period and save as .csv
#===============================================================================
waba_lt=pd.DataFrame()

for plot in inputParams.index:
    wbCompl=pd.read_csv(outputFolder+'/completeWB/'+plot+'.csv', sep='\t', converters={0:dateConverter}, index_col=0)
    wbCompl.dropna(subset=['dSsoil'], inplace=True)
    waba_lt[plot]=wbCompl[['dSsoil', 'dSsurf','eSoil', 'eSurf', 'infiltr_soil', 'ro', 'drainage', 'rain']].sum()

waba_lt.round(0).astype(int).to_csv(outputFolder+'/completeWB/longterm_waterBalances.csv', sep='\t') 


wabaSummary=waba_lt.loc[['rain', 'ro', 'dSsoil', 'drainage'],:].round(0).astype(int)
wabaSummary.loc['evap',:]=(waba_lt.loc[['eSoil', 'eSurf']].sum()).round(0).astype(int)
wabaSummary.to_csv(outputFolder+'/completeWB/longterm_waterBalances_summary.csv', sep='\t') 
