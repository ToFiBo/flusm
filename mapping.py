# -*- coding: utf-8 -*-
'''
=======================
module of FluSM for linking the surface and the soil module
=======================
'''
import pandas as pd
import numpy as np
import scipy.stats as stats
from datetime import datetime


def map_linScaling(mappingDf):
    """
    Function for mapping of two time series by applying by linear scaling (see Teutschbein & Seibert 2012)
    The scaling factor is determined for mapping infiltration gains of the soil module [vol.%/h] on infiltration of the surface module [mm/h]
    By applying the scaling factor on all fluxes of the soil module, their unit is transformed from [vol.%/h] to [mm/h] 
    
    Usage:
    ---------- 
    apply on pd.DataFrame grouped by time period (commonly months) to obtain time-variant scaling factors
    otherwise one scaling factor for the entire time series is obtained
    
    Parameters:
    ------------
    mappingDf: pd.DataFrame
        must include the columns [dSto_wc, infGains, infSurf, percLoss, evapLoss]
        dWc: change of soil moisture over time [vol.%/h]
        evapoLoss: losses of soil moisture due to evaporation [vol.%/h] (output of the soil module)
        drainageLoss: losses of soil moisture due to drainage [vol.%/h] (output of the soil module)
        infiltrGains: gains of soil moisture due to infiltration [vol.%/h] (output of the soil module)
        infiltr: infiltration into the soil [mm/h] (output of the surface module)
        ro: surface runoff [mm/h] (output of the surface water balance)
        eSurf: surface evaporation [mm/h] 
        
    Returns:
    ----------
        pd.Data.Frame including the fluxes of all water balance components [mm/h] as well as the used scaling factors
        if the function is applied onto a grouped dataFrame, the a multiindex is returned, which must be reset
    -----------
    """
    df=mappingDf.dropna(subset=['dWc'])
    df['evapoLoss'].fillna(0, inplace=True)
    df['drainageLoss'].fillna(0, inplace=True)

    try:
        slope, intercept, _, _, _ =stats.linregress(df['infiltrGains'].cumsum(), df['infiltr'].cumsum())
    except ValueError:
        slope, intercept=[np.NaN, np.NaN]
    
    soilFluxes=pd.DataFrame(data={'infiltr_soil':(df['infiltrGains']*slope),
                                  'infiltr_surf':df['infiltr'],
                                  'drainage':(df['drainageLoss']*slope),
                                  'dSsoil':(df['dWc']*slope),
                                  'eSoil':(df['evapoLoss']*slope),
                                  'scalingFact': [slope for i in range(len(df))],
                                  'dSsurf':df['dSsurf'],
                                  'eSurf':df['eSurf'],
                                  'ro':df['ro']})
    return soilFluxes



    